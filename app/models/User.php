<?php

class User extends Eloquent {
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'createdAt', 'updatedAt', 'socketid');

	protected $fillable = array('username','password', 'name', 'description');

}
