<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	public function show($username)
	{
		$user=User::where('username',$username)->first();
		if($user){
			return Response::json(['status'=>'fino','data'=>$user],200);
		}else{
			return Response::json(['status'=>'error'],404);
		}
	}

	public function getAll(){
		$data=Input::all();
		$arr=json_decode($data['lel']);
		$users=[];
		foreach ($arr as $uno) {
			$ll=User::where('username',$uno)->first();
			if($ll){
				$ll['exist']=true;
				array_push($users, $ll);
			}else{
				array_push($users, ['username'=>$uno,'exist'=>false]);
			}
		}
		return Response::json(['status'=>'fino','data'=>$users],200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
